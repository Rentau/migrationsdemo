﻿using System;
using System.Collections.Generic;

namespace EntityFrameworkCore
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public sealed class Movies
    {
        public Movies()
        {
            MovieCast = new HashSet<MovieCast>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MovieId { get; set; }

        [MaxLength(50)]
        public string Title { get; set; }

        [DataType(DataType.Text)]
        public string Summary { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int RuntimeMinutes { get; set; }

        public ICollection<MovieCast> MovieCast { get; set; }
    }
}
