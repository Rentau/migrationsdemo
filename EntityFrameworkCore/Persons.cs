﻿using System;
using System.Collections.Generic;

namespace EntityFrameworkCore
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public sealed class Persons
    {
        public Persons()
        {
            MovieCast = new HashSet<MovieCast>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PersonId { get; set; }

        public string FullName { get; set; }

        public DateTime Born { get; set; }

        public ICollection<MovieCast> MovieCast { get; set; }
    }
}
