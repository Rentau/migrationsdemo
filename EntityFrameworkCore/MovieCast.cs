﻿namespace EntityFrameworkCore
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class MovieCast
    {
        [Key, Column(Order = 0)]
        public int MovieId { get; set; }

        [Key, Column(Order = 1)]
        public int PersonId { get; set; }

        [Key, Column(Order = 2)]
        public string CharacterName { get; set; }

        [ForeignKey("MovieId")]
        [Required]
        public virtual Movies Movie { get; set; }

        [ForeignKey("PersonId")]
        [Required]
        public virtual Persons Person { get; set; }
    }
}
