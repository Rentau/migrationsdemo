﻿using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkCore
{
    public partial class SqlServerDatabaseContext : DbContext
    {
        public SqlServerDatabaseContext()
        {
        }

        public SqlServerDatabaseContext(DbContextOptions<SqlServerDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MovieCast> MovieCast { get; set; }
        public virtual DbSet<Movies> Movies { get; set; }
        public virtual DbSet<Persons> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
               optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=EntityFrameworkCore;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCast>().HasKey(_ => new { _.MovieId, _.PersonId, _.CharacterName });
        }
    }
}
