﻿CREATE TABLE [dbo].[movies]
(
	[movie_id] INT NOT NULL PRIMARY KEY, 
    [title] NVARCHAR(50) NOT NULL, 
    [summary] TEXT NULL, 
    [release_date] DATE NOT NULL, 
    [runtime_minutes] INT NOT NULL
)
