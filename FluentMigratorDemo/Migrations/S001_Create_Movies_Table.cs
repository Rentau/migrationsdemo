﻿namespace FluentMigratorDemo.Migrations
{
    using FluentMigrator;

    [Migration(1)]
    public class S001_Create_Movies_Table : AutoReversingMigration
    {
            public override void Up()
            {
                Create.Table("Movies")
                    .WithColumn("MovieId").AsInt32().NotNullable().PrimaryKey().Identity()
                    .WithColumn("Title").AsString(50).NotNullable()
                    .WithColumn("Summary").AsString().Nullable()
                    .WithColumn("ReleaseDate").AsDate().NotNullable()
                    .WithColumn("RuntimeMinutes").AsInt32().NotNullable();

                Create.Table("Persons")
                    .WithColumn("PersonId").AsInt32().NotNullable().PrimaryKey().Identity()
                    .WithColumn("FullName").AsString().NotNullable()
                    .WithColumn("Born").AsDate().NotNullable();

                Create.Table("Movie_cast")
                    .WithColumn("MovieId").AsInt32().NotNullable().PrimaryKey().ForeignKey("Movies", "MovieId")
                    .WithColumn("PersonId").AsInt32().NotNullable().PrimaryKey().ForeignKey("Persons", "PersonId")
                    .WithColumn("CharacterName").AsString(50).Nullable().PrimaryKey();
            }
    }
}