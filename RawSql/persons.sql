﻿CREATE TABLE [dbo].[persons]
(
	[person_id] INT NOT NULL PRIMARY KEY, 
    [full_name] NVARCHAR(MAX) NOT NULL, 
    [height] INT NOT NULL,
    [born] DATE NOT NULL
)
