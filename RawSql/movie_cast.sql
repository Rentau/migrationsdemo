﻿CREATE TABLE [dbo].[movie_cast]
(
    [movie_id] INT NOT NULL, 
    [person_id] INT NOT NULL, 
    [character_name] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [fk_movie_cast_movie_id_to_movies_movie_id] FOREIGN KEY ([movie_id]) REFERENCES [movies]([movie_id]) ON DELETE CASCADE,
    CONSTRAINT [fk_movie_cast_person_id_to_persons_person_id] FOREIGN KEY ([person_id]) REFERENCES [persons]([person_id]) ON DELETE CASCADE
)
